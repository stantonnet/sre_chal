package main

// SRE Programming Test 11/19/2017
// John Stanton
// TODO: Remove function is needed
//       More error checking
//       More testing
//       InstallPkg could be broken into more functions
//       General cleanup

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"errors"
)

var PackageDeps map[string][]string
var IsInstalled map[string]bool

// Adds packages and deps to PackageDeps
func AddPackage(pkg string) {
	depSlice := strings.Split(pkg, " ")
	pkgName := string(depSlice[1])
	pkgDeps := depSlice[2:]
	PackageDeps[pkgName] = pkgDeps
}

// Lists all installed packages by looking at IsInstalled map
func ListInstalled() (string, error) {
	if len(IsInstalled) == 0 {
		return "", errors.New("No packages installed.")
	}
	pkg := make([]string, 0)
	for k, _ := range IsInstalled {
		pkg = append(pkg, k)
	}
	output := strings.Join(pkg, " ")
	return output, nil
}

// Installs packages by using IsInstalled and PackageDeps map
func InstallPkg(pkg string) ([]string, bool) {
	pkg = strings.TrimPrefix(pkg, "INSTALL ")
	sorted := make([]string, 0)
	if _, ok := IsInstalled[pkg]; ok {
		installed := fmt.Sprintf("%s is already installed\n", pkg)
		sorted = append(sorted, installed)
		return sorted, true
	} else {
		// Install deps
		for _, v := range PackageDeps[pkg] {
			if _, ok := IsInstalled[v]; ok {
				continue
			}
			//last := PackageDeps[pkg][len(PackageDeps[pkg])-(1+k)]
			sorted = append(sorted, v)
		}
		sorted = append(sorted, pkg)
		for _, v := range sorted {
			IsInstalled[v] = true
		}
	}
	return sorted, false
}

func main() {
	// Setup maps
	PackageDeps = make(map[string][]string)
	IsInstalled = make(map[string]bool)

	//IsInstalled["foo"] = true
	//IsInstalled["DNS"] = true

	// This is where the magic happens
	reader := bufio.NewReader(os.Stdin)

	for {
		fmt.Print("-> ")
		text, _ := reader.ReadString('\n')
		text = strings.Replace(text, "\n", "", -1)

		switch {
		case strings.HasPrefix(text, "DEPEND"):
			fmt.Println(text)
			AddPackage(text)
		case strings.HasPrefix(text, "LIST"):
			output, err := ListInstalled()
			if err != nil {
				fmt.Printf("%s", err)
			}
			fmt.Println(output)
		case strings.HasPrefix(text, "INSTALL"):
			install, ok := InstallPkg(text)
			for _, v := range install {
				if ok {
					fmt.Printf("%s", v)
					continue
				}
				fmt.Printf("%s successfully installed\n", v)
			}
		case strings.HasPrefix(text, "REMOVE"):
			fmt.Println("remove")
		}
	}
}
